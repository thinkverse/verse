### Problem to solve

<!-- What problem do we solve? -->

### Intended users

<!-- Who will use this feature? If known, include any of the following: types of users (e.g. Admins), players, or specific player roles (e.g. Donators). It's okay to write "Unknown" and fill this field in later.-->

### Further details

<!-- Include use cases, benefits, and/or goals (contributes to the plugin?) -->

### Proposal

<!-- How are we going to solve the problem?-->

### Permissions and Security

<!-- What permissions are required to perform the described actions? Are they consistent with the existing permissions structure?-->

### Links / references

/label ~feature