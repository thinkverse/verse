package kim.hallberg.verse.utilities;

import com.google.common.base.Charsets;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Objects;
import java.util.logging.Level;

public final class Config {
  private final String filename;
  private final JavaPlugin plugin;

  private FileConfiguration configuration;
  private String folder = null;
  private File file;

  /**
   * @param plugin Current plugin
   * @param filename Name of config to be created
   * @param folder Name of the folder which holds the config
   */
  public Config(JavaPlugin plugin, String filename, String folder) {
    this.filename = filename;
    this.folder = folder;
    this.plugin = plugin;
  }

  /**
   * @param plugin Current plugin
   * @param filename Name of config to be created
   */
  public Config(JavaPlugin plugin, String filename) {
    this.filename = filename;
    this.plugin = plugin;
  }

  /**
   * Reload configuration
   */
  public void reloadConfig() {
    if (this.file == null) {
      this.file = new File(this.getDataFolder(), this.filename);
    }

    if (!this.file.exists()) {
      if (this.file.getParentFile().mkdirs()) {
        this.plugin.saveResource(this.filename, false);
      }
    }

    this.configuration = YamlConfiguration.loadConfiguration(this.file);

    final Reader configReader = new InputStreamReader(Objects.requireNonNull(this.plugin.getResource(this.filename)), Charsets.UTF_8);

    final YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(configReader);

    this.configuration.options().copyDefaults(true);
    this.configuration.setDefaults(defaultConfig);
  }

  /**
   * Save configuration
   */
  public void saveConfig() {
    if (Objects.isNull(this.configuration) || Objects.isNull(this.file)) return;

    try {
      this.getConfig().save(this.file);
    } catch (IOException exception) {
      plugin.getLogger().log(Level.SEVERE, "Could not save config to " + this.file, exception);
    }
  }

  /**
   * Save default configuration
   */
  public void saveDefaultConfig() {
    if (this.file == null) {
      this.file = new File(this.getDataFolder(), this.filename);
    }

    if (!this.file.exists()) {
      if (this.file.getParentFile().mkdirs()) {
        this.plugin.saveResource(this.filename, false);
      }
    }
  }

  /**
   * @return Get configuration
   */
  public FileConfiguration getConfig() {
    if (Objects.isNull(this.configuration)) reloadConfig();
    return this.configuration;
  }

  @NotNull
  private File getDataFolder() {
    return Objects.isNull(folder) ? this.plugin.getDataFolder() : new File(this.plugin.getDataFolder() + this.folder);
  }

  /**
   * Helper function to quickly build and save configuration file
   * @return Newly created configuration file
   */
  public Config build() {
    this.saveDefaultConfig();
    this.reloadConfig();
    this.saveConfig();

    return this;
  }
}
