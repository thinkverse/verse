package kim.hallberg.verse;

import kim.hallberg.verse.utilities.Chat;
import kim.hallberg.verse.utilities.Config;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.TabCompleter;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Verse extends JavaPlugin {
  final Config config = new Config(this, "config.yml").build();

  private void onStart() {
    if (!Objects.requireNonNull(getVersion()).startsWith("1.15")) {
      getLogger().severe(Chat.translate("&cThis can only run on MC: 1.15 servers."));
      getServer().getPluginManager().disablePlugin(this);
    }
  }

  @Override
  public void onEnable() { onStart(); }

  @Override
  public void onDisable() { }

  public Config config() { return config; }

  @Nullable
  protected final String getVersion() {
    Matcher matcher = Pattern.compile("(\\(MC: )([\\d\\.]+)(\\))").matcher(Bukkit.getVersion());
    if (matcher.find()) { return matcher.group(2); }
    return null;
  }

  protected final void registerEvents(Listener listener) {
    this.getServer().getPluginManager().registerEvents(listener, this);
  }

  protected final void registerCommand(String command, CommandExecutor executor, boolean tabCompleter) {
    Objects.requireNonNull(this.getCommand(command)).setExecutor(executor);
    if (tabCompleter) registerTabCompleter(command, (TabCompleter) executor);
  }

  protected final void registerTabCompleter(String command, TabCompleter completer) {
    Objects.requireNonNull(this.getCommand(command)).setTabCompleter(completer);
  }
}
